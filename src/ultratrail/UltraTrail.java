package ultratrail;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jordi San Nicolás
 */
public class UltraTrail {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        programa();
        
    }
    
    /**
     * Aquesta funció llença el programa principal
     * Està modularitzada per poder fer crides recursives en cas d'error
     */
    public static void programa() {
        
        Scanner lector = new Scanner(System.in);
        
        Cursa c1;        
        int numPasses;
        String tipusPasses;
        
        try {
        
            System.out.println("Introdueix el nombre de pases");
            numPasses = lector.nextInt();
            System.out.println("Introdueix la cadena que indica el tipus de cada passa");
            tipusPasses = lector.next();
        
            try {       

                c1 = new Cursa(numPasses, tipusPasses);
        
                if (c1.validaCursa()) {
                    
                    System.out.println("\n-------------------------------\n");                
                    System.out.println("El nombre de valls és: " + c1.calculaNumValls() + "\n");
                    c1.imprimeixGrafic();
                    
                }else {
                
                    System.out.println("Cursa invàlida");
                
                }
                
            } catch (IOException ex) {

                System.out.println(ex.getMessage());
                programa();
            }
        
        }catch(InputMismatchException ime) {
            
            System.out.println("Dades introduides incorrectes");
            programa();
            
        }
        
    }
    
}
