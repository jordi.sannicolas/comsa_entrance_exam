/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ultratrail;

import java.util.Random;
import static ultratrail.UltraTrail.programa;

/**
 *
 * @author Jordi
 */
public class GeneraCursaRandom {
    public static void main(String[] args) {
        
        int numPasses, nivell = 0;        
        Random r = new Random();
        boolean passa;
        String tipusPasses = "";
        
        numPasses = r.nextInt(100);
        numPasses += 4;
                        
        for (int i = 0; i < numPasses; i++) {
            
            passa = r.nextBoolean();
            
            if (passa) {
                
                tipusPasses += 'P';
                nivell++;
                
            }else {
                
                tipusPasses += 'B';
                nivell--;
            }
            
        }
        
        if (nivell < 0) {
            
            for (int i = 0; i < Math.abs(nivell); i++) {
                
                tipusPasses += 'P';
                numPasses++;
                
            }
            
        }else if (nivell > 0) {
            
            for (int i = 0; i < Math.abs(nivell); i++) {
                
                tipusPasses += 'B';
                numPasses++;
                
            }
            
        }
        
        System.out.println("Num passes: " + numPasses);        
        System.out.println("Cadena: " + tipusPasses);
        
    }
}
