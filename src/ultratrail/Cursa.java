package ultratrail;

import java.io.IOException;

/**
 *
 * @author Jordi San Nicolás
 */
public class Cursa {
    
    private int numPasses;
    private String tipusPasses;
    private char[][] matriuGrafic;
    
    /**
     * Constructor de la classe Cursa
     * @param numPasses nombre de passes que ha fet l'atleta a la cursa 
     * @param tipusPasses cadena de lletres que indica passa a passa si s'ha pujat o baixat amb lletres P i B
     * @throws IOException Si les dades introduïdes són incorrectes llença excepció. Això pot succeir en dos casos:
     * 1. El nombre de passes és diferent del nombre de lletres introduïdes
     * 2. El nombre de passes no es troba dins els límits
     */    
    public Cursa(int numPasses, String tipusPasses) throws IOException {
        
        if (numPasses >= 2 && numPasses <= Math.pow(10, 6)) {
        
            if (numPasses == tipusPasses.length()) {

                this.numPasses = numPasses;
                this.tipusPasses = tipusPasses;

            }else {

                throw new IOException("El nombre de passes no coincideix amb les dades introduides");

            }
        
        }else {
            
            throw new IOException("El nombre de passes no entra als límits");
            
        }
        
    }
    
    /**
     * Aquest métode comprova si la cursa comença i acaba al nivell del mar
     * @return si la cursa és vàlida o no
     */
    public boolean validaCursa() {
        
        boolean res = false;
        int nivell = 0;
        
        for (int i = 0; i < tipusPasses.length(); i++) {
            
            nivell = calculaCanviNivell(nivell, tipusPasses.charAt(i));
            
        }
        
        if (nivell == 0) {
            
            res = true;
            
        }
        
        return res;
        
    }
    
    /**
     * Funció que calcula el nombre de valls de la cursa
     * @return un enter amb el nombre de valls
     */
    public int calculaNumValls() {
        
        int cont = 0;
        int nivell = 0;
        
        for (int i = 0; i < tipusPasses.length(); i++) {
            
            // Es calcula el canvi de nivell
            nivell = calculaCanviNivell(nivell, tipusPasses.charAt(i));
            
            // Si la condició es compleix, és perquè s'ha passat d'estar en una vall a estar al nivell del mar
            if (nivell == 0 && tipusPasses.charAt(i) == 'P') {
                
                cont++;
                
            }
            
        }
        
        return cont;
    }
    
    /**
     * Funció que retorna el nivell d'alçada al que s'ha fet una passa
     * @param nivell fa referència al nivell previ a la passa
     * @param passa representa la lletra de la passa
     * @return el nivell posterior a la passa
     */
    private int calculaCanviNivell(int nivell, char passa) {
        
        int res = nivell;
        
        if (passa == 'B') {
                
            res--;
                
        }else if (passa == 'P') {
                
            res++;
                
        }
        
        return res;
        
    }
    
    /**
     * Funció que imprimeix el gráfic
     */
    public void imprimeixGrafic() {
        
        int nivell = calculaNivellMax();
        
        matriuGrafic = generaMatriuGrafic(matriuGrafic);
                
        for (int i = 0; i < matriuGrafic.length; i++) {                        
            
            for (int j = 0; j < matriuGrafic[0].length; j++) {
                
                System.out.print(matriuGrafic[i][j]);
                
            }
            
            System.out.println("");
                        
        }           
        
    }
    
    /**
     * Funció que crea un array amb el gràfic emmagatzemat per posició
     * @param m representa la matriu sobre la que es treballará
     * @return una matriu amb el gràfic de la cursa carregat
     */
    private char[][] generaMatriuGrafic(char[][] m) {
        
        int filaInicial = calculaFilaInicial();
        int filesGrafic = calculaFilesGrafic();
        int fila = filaInicial, columna = 0, max = calculaNivellMax();
        
        if (max == 0) {
            
            m = new char[filesGrafic + 1][tipusPasses.length() + 2];
             
        }else {
            
            m = new char[filesGrafic][tipusPasses.length() + 2];
           
        }
        
        System.out.println("Files: " + filesGrafic);
        System.out.println("Max: " + max);
        System.out.println("Min: " + calculaNivellMin());
        System.out.println("Fila inicial: "+ filaInicial);
        System.out.println("");
        
        m[fila][columna] = '_';
        m = assignaEspaisBlancs(m, columna, fila);
        columna++;        
               
        // Es recorre cada passa
        for (int i = 0; i < tipusPasses.length(); i++) {
                        
            // Es comprova que la passa actual no sigui la darrera, ja que es treballa amb l'index següent
            if (i < tipusPasses.length() - 1) {
                
                // Es comprova si la passa puja o baixa, ja que gràcies a això, es pot determinar la fila de la següent passa
                if (tipusPasses.charAt(i) == 'P') {
                    
                    m[fila][columna] = '/';                        
                    m = assignaEspaisBlancs(m, columna, fila);    
                    
                    // Si la següent passa també puja, ha d'anar colocada una fila amunt
                    if (tipusPasses.charAt(i+1) == 'P') {                  
                              
                        columna++;
                        fila--;  

                    // Si la següent passa baixa, ha d'anar colocada a la mateixa fila    
                    }else if (tipusPasses.charAt(i+1) == 'B') {
                        
                        columna++;

                    }

                }else if (tipusPasses.charAt(i) == 'B') { 
                     
                    // Correcció de fila després de '_'
                    if (i == 0) fila++; 
                    
                    m[fila][columna] = '\\';
                    m = assignaEspaisBlancs(m, columna, fila);
                        
                    // Si la següent passa també baixa, ha d'anar colocada una fila avall
                    if (tipusPasses.charAt(i+1) == 'B') {
                           
                        columna++;
                        fila++;

                    // Si la següent passa puja, ha d'anar colocada a la mateixa fila   
                    }else if (tipusPasses.charAt(i+1) == 'P') {     

                        columna++;

                    }

                }
                
            }else {
                
                if (tipusPasses.charAt(i) == 'P') {
                    
                    m[fila][columna] = '/';                        
                    m = assignaEspaisBlancs(m, columna, fila);    
                    
                }else if (tipusPasses.charAt(i) == 'B') { 
                     
                    m[fila][columna] = '\\';
                    m = assignaEspaisBlancs(m, columna, fila);                     

                }
                
            }
                        
        }         
         
        //Si la darrera passa es pujada, es resta 1 a la fila per posar la '_'                
        if (m[fila][columna] == '/') fila--;
        
        // Per dibuixar la darrera '_', cal avançar una columna
        columna++;  
                       
        if (fila == filaInicial) {            
            
            m[filaInicial][columna] = '_';
            m = assignaEspaisBlancs(m, columna, filaInicial);
            
        }
        
        return m;
        
    }
    
    /**
     * Funció per calcular la fila en la que el guarda el primer caracter
     * @return el index de la fila
     */
    private int calculaFilaInicial() {
    
        int filaInicial = 0;
        int max = calculaNivellMax();
        
        if (max > 1) {
            
            filaInicial = calculaNivellMax() - 1;
            
        }
        
        return filaInicial;
        
    }
    
    /**
     * Funció que omple d'espais en blanc les cel·les buides de la columna 
     * indicada, excepte la cel·la en què es guarda el signe '/' o '\'
     * @param m array sobre el que es treballa
     * @param col columna a omplir d'espais
     * @param filaBona fila on la cel·la s'ha de respectar
     * @return un array amb les dades actualitzades
     */
    private char[][] assignaEspaisBlancs(char[][] m, int col, int filaBona) {
        
        for (int i = 0; i < m.length; i++) {
                            
            if (i != filaBona) {

                m[i][col] = ' ';

            }

        }
        
        return m;
    }
    
    /**
     * Funció que calcula les fileres que hi haurà al gràfic
     * @return la diferència entre el nivell màxim i el mínim
     */
    private int calculaFilesGrafic() {

        int files, max = calculaNivellMax(), min = calculaNivellMin();

        if (max == 0) {
            
            files = max - min + 1;
            
        }else {
            
            files = max - min;
            
        }
        
        return files;
                
    }
    
    /**
     * Funció que calcula el nivell màxim del gráfic
     * @return el valor màxim trobat
     */
    private int calculaNivellMax() {
        
        int nivell = 0, nivellMax = 0;
        
        for (int i = 0; i < tipusPasses.length(); i++) {
            
            nivell = calculaCanviNivell(nivell, tipusPasses.charAt(i));
                        
            if (nivell > nivellMax) {                
                nivellMax = nivell;                
            }
        }
        
        return nivellMax;
        
    }
    
    /**
     * Funció que calcula el nivell mínim del gràfic
     * @return el nivell mínim trobat
     */
    private int calculaNivellMin() {
        
        int nivell = 0, nivellMin = 0;
        
        for (int i = 0; i < tipusPasses.length(); i++) {
            
            nivell = calculaCanviNivell(nivell, tipusPasses.charAt(i));
                        
            if (nivell < nivellMin) {                
                nivellMin = nivell;                
            }
        }
        
        return nivellMin;
        
    }
    
    /**
     * Setters i getters
     */
    
    public int getNumPasses() {
        return numPasses;
    }

    public void setNumPasses(int numPasses) {
        this.numPasses = numPasses;
    }

    public String getTipusPasses() {
        return tipusPasses;
    }

    public void setTipusPasses(String tipusPasses) {
        this.tipusPasses = tipusPasses;
    }
    
    
    
}
